
;(function(){

	const _qs = new URLSearchParams(location.search);
	let _settings;

	const _fn = {

		/**
		 * @description Start up the extension.
		 * @returns {undefined}
		 */
		init() {
			_fn.setExtensionSettings();

			// Poll for settings to populate.
			const pollSettings = setInterval(function() {
				if (_settings) {
					clearInterval(pollSettings);
					_fn.updateQueryString();
				}
			}, 100);
		},

		/**
		 * @description Get the extension settings and set to the `_settings` var.
		 * @returns {undefined}
		 */
		setExtensionSettings() {
			chrome.storage.sync.get({
					hideWS: true,
					tabSpace: 4,
					useTabs: false
				},
				(settings) => {
					_settings = settings;
				}
			);
		},

		/**
		 * @description Update any query string parameters as needed.
		 * @returns {undefined}
		 */
		updateQueryString() {
			const hasTabs = _qs.has('ts');
			const hasWhites = _qs.has('w');

			const setTabs = !hasTabs && _settings.useTabs;
			const setWhites = !hasWhites && _settings.hideWS;

			if (setTabs || setWhites) {
				if (setTabs) _qs[hasTabs ? 'set' : 'append']('ts', _settings.tabSpace);
				if (setWhites) _qs[hasWhites ? 'set' : 'append']('w', 1);

				const url = `${location.pathname}?${_qs.toString()}${location.hash}`;

				location.replace(url);
			}
		},
	};

	_fn.init();
})();
